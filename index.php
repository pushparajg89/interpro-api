<?php 
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Views\PhpRenderer;
ini_set('display_error','1');
require 'vendor/autoload.php';
$app = new \Slim\App;
$container = $app->getContainer();
$container['renderer'] = new PhpRenderer('./templates');
//$app->get('/', '\ApiController:home');

//loading template
$app->get('/',function ($request, $response, $args) use ($app){
	return $this->renderer->render($response, "/api_form.html");
	});
	
	
$app->get('/findByStatus/{status}', '\ApiController:findPetByStatus');
$app->get('/pet/{id}', '\ApiController:getPetDetailsById');
$app->get('/post_pet/{id}/{name}/{status}', '\ApiController:petPost');
$app->get('/delete_pet/{id}/{apikey}', '\ApiController:deletePet');


class ApiController {
	
	public function home($request, $response, $args) {
		 $response->write("Welcome to API Testing!");
		return $app->render('api_form.php');
         return $response;
	}
	
	/*
	Retriving Pet Details By Status
	**/
	public function findPetByStatus(Request $request, Response $response, array $args) {
		$name = $args['status'];
		$url = "http://petstore.swagger.io/v2/pet/findByStatus?status=".$name;
		$method = "GET";
		$postValues = false;
		$this->curlCall ($url,$method,$postValues,$response);
	}
	
	/*
	Retriving Pet Details By ID
	**/
	public function getPetDetailsById(Request $request, Response $response, array $args) {
		$id = $args['id'];
		$url = "http://petstore.swagger.io/v2/pet/".$id;
		$method = "GET";
		$postValues = false;
		$this->curlCall ($url,$method,$postValues,$response);
	   
	}

	/*
	Updating Pet from Pet Store
	**/
	public function petPost(Request $request, Response $response, array $args) {
		$id = $args['id'];
		$name = $args['name'];
		$status = $args['status'];
		$postValues = "name=".$name."&status=".$status;
		$url = "http://petstore.swagger.io/v2/pet/".$id;
		$method = "POST";
		$this->curlCall ($url,$method,$postValues,$response);
	}

	/*
	Function for Deleting PET from Pet Store Swagger.
	**/
	public function deletePet(Request $request, Response $response, array $args) {
		$id = $args['id'];
		$apikey = $args['apikey'];
		$postValues = "api_key: ".$apikey;
		$url = "http://petstore.swagger.io/v2/pet/".$id;
		$method = "DELETE";
		$this->curlCall ($url,$method,$postValues,$response);
	}

	/*
	CURL General Function
	**/
	public function curlCall ($url,$method,$postValues,$response) {
		$ch = curl_init();
		$apiKey = "testjumboapi";
		curl_setopt($ch, CURLOPT_URL, $url);
		if($method == "DELETE") {
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
		}
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		if($method == "DELETE" || $method == "POST") {
			curl_setopt($ch, CURLOPT_POSTFIELDS,$postValues);
		}
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Authorization: ' . $apiKey
		));
		$output = curl_exec($ch);
		$clsCurl = curl_close($ch); 
		$response->getBody()->write("Request Successful<pre>".json_encode(json_decode($output), JSON_PRETTY_PRINT));
	}
}

$app->run();

?>